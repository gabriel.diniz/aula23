public class aula23 { //associatividade de operadores

    public static void main(String[] args) {

        int x,y,z;  // variaveis tipo inteira
        x = (y = (z = 0) );

        int a = (x + y) + z;

        /* níveis de associação
        esquerda para direita>>>>>>>>>>
         * 1) ()
         * 2) *, /, %
         * 3) +, -
         * 4) <, <=, >, >=
         * 5) ==, !=

         direita para esquerda<<<<<<<<<<
           1) = operador de atribuição
         */

    }
}
